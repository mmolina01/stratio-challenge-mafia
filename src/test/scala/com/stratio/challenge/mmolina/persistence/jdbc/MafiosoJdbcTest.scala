package com.stratio.challenge.mmolina.persistence.jdbc

import com.stratio.challenge.mmolina.common.Utils
import com.stratio.challenge.mmolina.persistence.Mafioso
import com.stratio.challenge.mmolina.services.MafiosoService.{defaultPage, defaultPageSize, defaultSortField}

class MafiosoJdbcTest extends BaseJdbcTest {

  import com.stratio.challenge.mmolina._

  lazy val dao = new MafiosoJdbc(dbConfig)
  val maxRecords = 10

  override def beforeEach = {
    super.beforeEach
    import dbConfig.profile.api._
    dbConfig.db.run(dao.mafiosos.delete).futureValue
    dbConfig.db.run(dao.prisonRecords.delete).futureValue
    dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue.size should be(0)
  }

  override def afterEach = {
    super.afterEach
    import dbConfig.profile.api._
    dbConfig.db.run(dao.mafiosos.delete).futureValue
    dbConfig.db.run(dao.prisonRecords.delete).futureValue
    dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue.size should be(0)
  }

  "A Mafioso JDBC" should {

    val sample = new Mafioso(
      name = "test",
      surname = "test",
      birthDate = None)

    "add and delete new mafioso" in {
      dao.add(None, sample).futureValue.isDefined should be(true)
      val records = dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue
      records.size should be(1)
      records.head.name should be("test")
      records.head.surname should be("test")
      records.head.createdAt shouldNot be(None)
      records.head.path should be("/")

      dao.delete(records.head.id.get.id).futureValue should be(1)
      dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue.size should be(0)

      dao.add(Some(1), sample).futureValue.isDefined should be(false)
      dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue.size should be(0)
    }

    "find a mafioso by id and update him" in {
      dao.add(None, sample).futureValue.isDefined should be(true)
      val records = dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue
      records.size should be(1)

      addTestData
      dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue.size should be(11)

      var result = dao.findById(records.head.id.get.id).futureValue
      result.isDefined should be(true)
      result.get.name should be(sample.name)
      result.get.surname should be(sample.surname)
      result.get.path should be("/")
      result.get.createdAt shouldNot be(None)
      result.get.updatedAt should be(None)

      dao.update(result.get.copy(name = "updated", surname = "updated")).futureValue should be(Some(1))
      result = dao.findById(records.head.id.get.id).futureValue
      result.isDefined should be(true)
      result.get.name should be("updated")
      result.get.surname should be("updated")
      result.get.path should be("/")
      result.get.createdAt shouldNot be(None)
      result.get.updatedAt shouldNot be(None)
    }

    "get the subordinates" in {
      addTestData

      val result = dao.subordinates("/").futureValue
      result.size should be(0)

      val records = dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue
      records.size should be(10)

      dao.subordinates(records.head.path).futureValue.size should be(0)
      dao.add(Some(records.head.id.get.id), sample).futureValue.get should be(1)
      dao.subordinates(records.head.path).futureValue.size should be(1)
    }

    "imprison and release a mafioso" in {
      addTestData

      dao.subordinates("/").futureValue.size should be(0)

      val records = dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue
      records.size should be(maxRecords)

      dao.imprison(records.head.id.get.id).futureValue shouldNot be(None)
      dao.imprison(records.head.id.get.id).futureValue should be(None)
      dao.subordinates("/").futureValue.size should be(0)

      val candidate = records.last
      dao.subordinates(Utils.withPath(candidate.id.get.id.toString, "")).futureValue.size should be(0)
      val records1 = dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue
      dao.add(Some(candidate.id.get.id), sample).futureValue.get should be(1)
      val records2 = dao.selectAll(defaultPage, defaultPageSize, defaultSortField).futureValue
      dao.subordinates(Utils.withPath(candidate.id.get.id.toString, "")).futureValue.size should be(1)

      dao.imprison(candidate.id.get.id).futureValue shouldNot be(None)
      dao.imprison(candidate.id.get.id).futureValue should be(None)
      dao.subordinates(Utils.withPath(candidate.id.get.id.toString, "")).futureValue.size should be(0)

      dao.release(candidate.id.get.id).futureValue shouldNot be(None)
      dao.release(candidate.id.get.id).futureValue should be(None)
      dao.subordinates(Utils.withPath(candidate.id.get.id.toString, "")).futureValue.size should be(1)
    }
  }

  private def addTestData = {
    (1 to maxRecords).foreach { index =>
      val mafioso = Mafioso(name = "name" + index, surname = "surname" + index, birthDate = None)
      dao.add(None, mafioso).futureValue
    }
  }

}

package com.stratio.challenge.mmolina.persistence.jdbc

import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Matchers, WordSpec}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

class BaseJdbcTest extends WordSpec
  with Matchers
  with ScalaFutures
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  implicit val patience = PatienceConfig(timeout = Span(5, Seconds), interval = Span(500, Millis))

  protected val config = ConfigFactory.parseString(
    """
      |    h2 {
      |      profile = "slick.jdbc.H2Profile$"
      |      db {
      |        dataSourceClass = "org.h2.jdbcx.JdbcDataSource"
      |        properties = {
      |          url = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"
      |        }
      |      }
      |    }
    """.stripMargin
  )

  private[jdbc] val dbConfig: DatabaseConfig[JdbcProfile] = DatabaseConfig.forConfig("h2", config)

  override protected def beforeAll = {
    super.beforeAll
    cleanDb
  }

  override protected def afterAll = {
    super.afterAll
    cleanDb
  }

  protected def cleanDb = {
    import dbConfig.profile.api._
    dbConfig.db.run(sqlu"DROP ALL OBJECTS").futureValue
  }

}

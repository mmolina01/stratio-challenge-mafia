package com.stratio.challenge.mmolina.persistence.dao

import com.stratio.challenge.mmolina.common.Constants
import com.stratio.challenge.mmolina.domain.MafiosoId
import com.stratio.challenge.mmolina.persistence.Mafioso

import scala.concurrent.Future

class MockMafiosoDao extends MafiosoDao {

  override def add(boss: Option[Int], mafioso: Mafioso): Future[Option[Int]] = Future.successful(Some(1))

  override def update(mafioso: Mafioso): Future[Option[Int]] = Future.successful(testData.get(mafioso.id.get.id).map(_.id.get.id))

  override def delete(id: Int): Future[Int] = Future.successful(testData.get(id).map(_.id.get.id).getOrElse(-1))

  override def selectAll(page: Int, pageSize: Int, sort: String): Future[Seq[Mafioso]] = {
    Future.successful(testData.values.toSeq)
  }

  override def findById(id: Int): Future[Option[Mafioso]] = Future.successful(testData.get(id))

  override def subordinates(path: String): Future[Seq[Mafioso]] = Future.successful(testData.values.toSeq)

  override def imprison(id: Int): Future[Option[Int]] = Future.successful(testData.get(id).map(_.id.get.id))

  override def release(id: Int): Future[Option[Int]] = Future.successful(testData.get(id).map(_.id.get.id))

  override private[dao] def createSchema = Future.successful(Unit)

  val testData = Map(
    1 -> Mafioso(id = Some(MafiosoId(1)), name = "test1", surname = "test1", birthDate = None),
    2 -> Mafioso(id = Some(MafiosoId(2)), name = "test2", surname = "test2", birthDate = None,
      path = s"${Constants.PathSeparator}/1")
  )

}

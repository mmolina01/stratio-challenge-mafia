package com.stratio.challenge

import akka.actor.ActorSystem

import scala.concurrent.ExecutionContext

package object mmolina {

  implicit val system = ActorSystem("test-system")
  implicit val executor: ExecutionContext = system.dispatcher

}

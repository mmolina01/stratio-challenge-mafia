package com.stratio.challenge.mmolina.services

import com.stratio.challenge.mmolina._
import com.stratio.challenge.mmolina.domain.MafiosoEntryPoint
import com.stratio.challenge.mmolina.persistence.dao.MockMafiosoDao
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}

class MafiosoServiceTest extends WordSpec with Matchers with ScalaFutures {

  "A Mafioso service" should {

    val service = new MafiosoService(dao = new MockMafiosoDao)

    val data = MafiosoEntryPoint(name = "test", surname = "test")

    "register a new mafioso" in {
      service.register(data).futureValue should be(Some(1))
    }

    "unregister a new mafioso" in {
      service.unregister(1).futureValue should be(1)
      service.unregister(999).futureValue should be(-1)
    }

    "update a mafioso if exists" in {
      service.update(1, data).futureValue should be(Some(1))
      service.update(999, data).futureValue should be(None)
    }

    "get all mafiosos" in {
      service.getAll(None, None, None).futureValue.size should be(2)
    }

    "get a mafioso by id" in {
      val result = service.get(1).futureValue
      result.isDefined should be(true)
      result.get.name should be("test1")

      service.get(999).futureValue.isDefined should be(false)
    }

    "get mafioso's subordinates" in {
      val result = service.subordinates(1).futureValue
      result.isDefined should be(true)
      result.get.size should be(2)

      service.subordinates(999).futureValue.isDefined should be(false)
    }

    "put a mafioso in prison" in {
      val result = service.imprison(1).futureValue
      result.isDefined should be(true)
      result.get should be(1)

      service.imprison(999).futureValue.isDefined should be(false)
    }

    "release a mafioso from prison" in {
      val result = service.release(1).futureValue
      result.isDefined should be(true)
      result.get should be(1)

      service.release(999).futureValue.isDefined should be(false)
    }

  }

}

package com.stratio.challenge.mmolina.routes

import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.stratio.challenge.mmolina.domain.{MafiosoEntryPoint, MafiosoResponse}
import com.stratio.challenge.mmolina.persistence.dao.MockMafiosoDao
import com.stratio.challenge.mmolina.services.MafiosoService
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}

class MafiosoRouteTest extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest with MafiosoRoute {

  "Mafioso routes" should {

    lazy val routes = mafioso
    val data = MafiosoEntryPoint(name = "test", surname = "test")

    "return all mafiosos in a GET request" in {
      Get("/mafiosos") ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType shouldEqual ContentTypes.`application/json`
        val result = entityAs[Seq[MafiosoResponse]]
        result.size should ===(2)
        result.head.id should ===(1)
        result.head.createdAt should !==(None.orNull)
      }
    }

    "get a mafioso in a GET request" in {
      Get("/mafiosos/2") ~> routes ~> check {
        contentType shouldEqual ContentTypes.`application/json`
        entityAs[MafiosoResponse].name should ===("test2")
        status should ===(StatusCodes.OK)
      }

      Get("/mafiosos/999") ~> routes ~> check {
        status should ===(StatusCodes.NotFound)
      }
    }

    "get mafioso's subordinates in a GET request" in {
      Get("/mafiosos/2/subordinates") ~> routes ~> check {
        status should ===(StatusCodes.OK)
        val result = entityAs[Seq[MafiosoResponse]]
        result.size should ===(2)
        result.head.id should ===(1)
        result.head.createdAt should !==(None.orNull)
      }
    }

    "add a mafioso in a POST request" in {
      Post("/mafiosos", data) ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }

    "put a mafioso in prison a POST request" in {
      Post("/mafiosos/1/imprison") ~> routes ~> check {
        status should ===(StatusCodes.OK)
      }

      Post("/mafiosos/999/imprison") ~> routes ~> check {
        status should ===(StatusCodes.Forbidden)
      }
    }

    "release a mafioso from prison a POST request" in {
      Post("/mafiosos/1/release") ~> routes ~> check {
        status should ===(StatusCodes.OK)
      }

      Post("/mafiosos/999/release") ~> routes ~> check {
        status should ===(StatusCodes.Forbidden)
      }
    }

    "update a mafioso in a PUT request" in {
      Put("/mafiosos/1", data) ~> routes ~> check {
        status should ===(StatusCodes.OK)
      }

      Put("/mafiosos/1", data.copy(boss = Some(1))) ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
      }

      Put("/mafiosos/999", data.copy(boss = Some(1))) ~> routes ~> check {
        status should ===(StatusCodes.BadRequest)
      }
    }

    "delete a mafioso in a DELETE request" in {
      Delete("/mafiosos/2") ~> routes ~> check {
        status should ===(StatusCodes.OK)
      }
    }

  }

  override def mafiosoService: MafiosoService = new MafiosoService(dao = new MockMafiosoDao)

}

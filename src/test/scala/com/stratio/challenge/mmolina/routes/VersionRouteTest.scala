package com.stratio.challenge.mmolina.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.stratio.challenge.mmolina.domain.Version
import com.stratio.challenge.mmolina.version.MafiaBuildInfo
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}

class VersionRouteTest extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest with VersionRoute {

  "Version routes" should {

    lazy val routes = version

    "return the version in a GET request" in {
      Get("/version") ~> routes ~> check {
        status should ===(StatusCodes.OK)
        entityAs[Version].version should ===(MafiaBuildInfo.version)
      }
    }

  }

}

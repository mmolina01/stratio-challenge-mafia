package com.stratio.challenge.mmolina.persistence.dao

import com.stratio.challenge.mmolina.persistence.Mafioso

import scala.concurrent.Future

trait MafiosoDao {

  private[dao] def createSchema: Future[Unit]

  def add(boss: Option[Int], mafioso: Mafioso): Future[Option[Int]]

  def update(mafioso: Mafioso): Future[Option[Int]]

  def delete(id: Int): Future[Int]

  def selectAll(page: Int, pageSize: Int, sort: String): Future[Seq[Mafioso]]

  def findById(id: Int): Future[Option[Mafioso]]

  def subordinates(path: String): Future[Seq[Mafioso]]

  def imprison(id: Int): Future[Option[Int]]

  def release(id: Int): Future[Option[Int]]

}

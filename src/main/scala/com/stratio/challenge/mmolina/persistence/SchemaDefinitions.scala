package com.stratio.challenge.mmolina.persistence

import java.sql.Timestamp
import java.time.{Instant, LocalDate}

import com.stratio.challenge.mmolina.common.Constants
import com.stratio.challenge.mmolina.domain.MafiosoId
import com.stratio.challenge.mmolina.persistence.Status.PrisonStatus
import slick.jdbc.JdbcProfile

case class Mafioso(
                    id: Option[MafiosoId] = None,
                    name: String,
                    surname: String,
                    path: String = Constants.PathSeparator.toString,
                    birthDate: Option[LocalDate],
                    status: PrisonStatus = Status.Free,
                    createdAt: Timestamp = Timestamp.from(Instant.now),
                    updatedAt: Option[Timestamp] = None
                  )

object Status extends Enumeration {
  type PrisonStatus = Value
  val Free = Value(1)
  val Imprisoned = Value(2)

  def withId(id: Int): PrisonStatus = {
    Status.values.find(_.id == id).getOrElse(throw new IllegalArgumentException(s"Unknown status: $id"))
  }

}

case class PrisonRecord(
                         id: Option[MafiosoId],
                         exsubordinates: List[MafiosoId],
                         createdAt: Timestamp = Timestamp.from(Instant.now),
                         releasedAt: Option[Timestamp] = None
                       )

trait SchemaDefinitions {

  val jdbcProfile: JdbcProfile

  import jdbcProfile.api._

  implicit lazy val mafiosoIdColumnType = MappedColumnType.base[MafiosoId, Int](_.id, MafiosoId(_))
  implicit lazy val statusColumnType = MappedColumnType.base[PrisonStatus, Int](_.id, Status.withId(_))

  class Mafiosos(tag: Tag) extends Table[Mafioso](tag, "MAFIOSO") {

    def id = column[Option[MafiosoId]]("ID", O.PrimaryKey, O.AutoInc)

    def name = column[String]("NAME")

    def surname = column[String]("SURNAME")

    def path = column[String]("PATH")

    def birthDate = column[Option[LocalDate]]("BIRTH_DATE")

    def status = column[PrisonStatus]("STATUS")

    def createdAt = column[Timestamp]("CREATED_AT", O.Default(Timestamp.from(Instant.now)))

    def updatedAt = column[Option[Timestamp]]("UPDATED_AT")

    def * = (id, name, surname, path, birthDate, status, createdAt, updatedAt) <> ((construct _).tupled, extract)

    def idx = index("idx_path", path, unique = false)

    private def construct(id: Option[MafiosoId], name: String, surname: String, path: String,
                          birthDate: Option[LocalDate], status: PrisonStatus,
                          createdAt: Timestamp, updatedAt: Option[Timestamp]): Mafioso = {
      Mafioso(id, name, surname, path, birthDate, status, createdAt, updatedAt)
    }

    private def extract(m: Mafioso): Option[(Option[MafiosoId], String, String, String,
      Option[LocalDate], PrisonStatus, Timestamp, Option[Timestamp])] = {
      Some((m.id, m.name, m.surname, m.path, m.birthDate, m.status, m.createdAt, m.updatedAt))
    }

  }

  class PrisonRecords(tag: Tag) extends Table[PrisonRecord](tag, "PRISON_RECORD") {

    def id = column[Option[MafiosoId]]("ID")

    def exsubordinates = column[String]("EXSUBORDINATES")

    def createdAt = column[Timestamp]("CREATED_AT", O.Default(Timestamp.from(Instant.now)))

    def releasedAt = column[Option[Timestamp]]("RELEASED_AT")

    def * = (id, exsubordinates, createdAt, releasedAt) <> ((construct _).tupled, extract)

    def idFk = foreignKey("mafioso_fk", id, TableQuery[Mafiosos])(_.id, onDelete = ForeignKeyAction.Cascade)

    private def construct(id: Option[MafiosoId], exsubordinates: String,
                          createdAt: Timestamp, releasedAt: Option[Timestamp]): PrisonRecord = {
      PrisonRecord(id, exsubordinates.split('|').filterNot(_.isEmpty).map(id => MafiosoId(id.toInt)).toList,
        createdAt, releasedAt)
    }

    private def extract(jr: PrisonRecord): Option[(Option[MafiosoId], String, Timestamp, Option[Timestamp])] = {
      Some((jr.id, jr.exsubordinates.map(_.id).mkString("|"), jr.createdAt, jr.releasedAt))
    }

  }

  lazy val mafiosos = TableQuery[Mafiosos]
  lazy val prisonRecords = TableQuery[PrisonRecords]

}

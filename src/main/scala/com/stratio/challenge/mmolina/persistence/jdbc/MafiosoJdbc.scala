package com.stratio.challenge.mmolina.persistence.jdbc

import java.sql.Timestamp
import java.time.Instant

import com.stratio.challenge.mmolina.common.{Constants, Utils}
import com.stratio.challenge.mmolina.domain.MafiosoId
import com.stratio.challenge.mmolina.persistence._
import com.stratio.challenge.mmolina.persistence.dao.MafiosoDao
import slick.basic.DatabaseConfig
import slick.dbio.DBIOAction
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

class MafiosoJdbc(protected val dbConfig: DatabaseConfig[JdbcProfile])(implicit val ec: ExecutionContext)
  extends MafiosoDao with SchemaDefinitions {

  import dbConfig.profile.api._

  override val jdbcProfile = dbConfig.profile
  implicit lazy val mafiosoIdColumnType2 = MappedColumnType.base[MafiosoId, Int](_.id, MafiosoId(_))
  val db = dbConfig.db

  createSchema

  override def createSchema: Future[Unit] = {
    db.run(mafiosos.schema.createIfNotExists andThen prisonRecords.schema.createIfNotExists)
  }

  private val sortMap = Map(
    "ID" -> mafiosos.sortBy(_.id),
    "NAME" -> mafiosos.sortBy(_.name),
    "SURNAME" -> mafiosos.sortBy(_.surname),
    "PATH" -> mafiosos.sortBy(_.path),
    "BIRTH_DATE" -> mafiosos.sortBy(_.birthDate)
  )

  private def upsert(bossId: Option[Int], mafioso: Mafioso)(op: Mafioso => Future[Int]): Future[Option[Int]] = {
    bossId.map { bid =>
      findById(bid).collect[Future[Option[Int]]] {
        case Some(boss) if (mafioso.id == boss.id) =>
          Future.failed(new RuntimeException("A mafioso and his boss cannot be same."))
        case Some(boss) =>
          val child = mafioso.copy(path = Utils.withPath(boss.path, boss.id.get.id.toString))
          op(child).map(Some(_))
        case _ => Future.successful(None)
      }.flatten
    }.getOrElse(op(mafioso.copy(path = Constants.PathSeparator.toString)).map(Some(_)))
  }

  override def add(boss: Option[Int], mafioso: Mafioso): Future[Option[Int]] = {
    upsert(boss, mafioso)(m => db.run(mafiosos += m))
  }

  override def update(mafioso: Mafioso): Future[Option[Int]] = {
    if (mafioso.id.isEmpty) {
      Future.successful(None)
    } else {
      upsert(Utils.getBoss(mafioso.path), mafioso) { m =>
        db.run(mafiosos.filter(_.id === mafioso.id)
          .update(m.copy(updatedAt = Some(Timestamp.from(Instant.now)))))
      }
    }
  }

  override def delete(id: Int): Future[Int] = db.run(mafiosos.filter(_.id === MafiosoId(id)).delete)

  override def selectAll(page: Int, pageSize: Int, sort: String): Future[Seq[Mafioso]] = {
    sortMap.get(sort.toUpperCase) match {
      case Some(query) => db.run(query.drop(page * pageSize).take(pageSize).result)
      case None => Future.failed(new RuntimeException(s"Cannot sort by field '$sort'."))
    }
  }

  override def findById(id: Int): Future[Option[Mafioso]] =
    db.run(mafiosos.filter(_.id === MafiosoId(id)).result.headOption)

  override def subordinates(path: String): Future[Seq[Mafioso]] = {
    val parent = Utils.withPath(path, "")
    db.run {
      mafiosos
        .filter(_.path like s"$parent%")
        .filter(_.path =!= Constants.PathSeparator.toString)
        .filterOpt(Utils.getBoss(path))((m, v) => m.id =!= MafiosoId(v))
        .filter(_.status === Status.Free)
        .result
    }
  }

  override def imprison(id: Int): Future[Option[Int]] = {
    // all this logic must be transactional
    val query = for {
      mafioso <- mafiosos.filter(_.id === MafiosoId(id)).filter(_.status === Status.Free).result.headOption

      sibling <- if (mafioso.isDefined) {
        mafiosos.filter(_.path === mafioso.get.path).filter(_.id =!= MafiosoId(id)).filter(_.status === Status.Free)
          .sortBy(_.birthDate.desc)
          .result.headOption
      } else DBIOAction.successful(None)

      subordinates <- if (mafioso.isDefined) {
        mafiosos.filter(_.path like s"${Utils.withPath(mafioso.get.path, mafioso.get.id.get.id.toString)}%")
          .filter(_.id =!= MafiosoId(id))
          .filter(_.status === Status.Free)
          .sortBy(_.birthDate.desc)
          .result
      } else DBIOAction.successful(Seq.empty)

      _ <- if (sibling.isDefined) {
        val targetPath = Utils.withPath(sibling.get.path, sibling.get.id.get.id.toString)
        DBIOAction.sequence(subordinates.map { m =>
          val resultPath = {
            val currentPath = m.path
            val imprisonedPath = Utils.withPath(mafioso.get.path, mafioso.get.id.get.id.toString)
            currentPath.replaceFirst(imprisonedPath, targetPath)
          }
          mafiosos.filter(_.id === m.id)
            .update(m.copy(path = resultPath,
              updatedAt = Some(Timestamp.from(Instant.now))))
        })
      } else if (subordinates.nonEmpty) {
        val newBoss = subordinates.head
        DBIOAction.sequence(subordinates.map { m =>
          val targetPath = if (m.id == newBoss.id) {
            mafioso.get.path
          } else {
            Utils.withPath(newBoss.path, newBoss.id.get.id.toString)
          }
          val resultPath = {
            val currentPath = m.path
            val imprisonedPath = Utils.withPath(mafioso.get.path, mafioso.get.id.get.id.toString)
            currentPath.replaceFirst(imprisonedPath, targetPath)
          }
          mafiosos.filter(_.id === m.id)
            .update(m.copy(path = resultPath,
              updatedAt = Some(Timestamp.from(Instant.now))))
        })
      } else DBIOAction.successful(Seq.empty)

      _ <- if (mafioso.isDefined) {
        val directSubordinates = subordinates.toList
          .filter(s => Utils.getBoss(s.path).map(MafiosoId(_)) == mafioso.get.id).map(_.id.get)
        DBIOAction.seq(
          prisonRecords += PrisonRecord(mafioso.get.id, directSubordinates),
          mafiosos.filter(_.id === mafioso.get.id).update(mafioso.get.copy(
            status = Status.Imprisoned, updatedAt = Some(Timestamp.from(Instant.now))))
        )
      } else DBIOAction.successful(mafioso)

    } yield mafioso

    db.run(query.transactionally).map(_.flatMap(_.id.map(_.id)))
  }

  override def release(id: Int): Future[Option[Int]] = {
    val query = for {
      prisoned <- (mafiosos.filter(_.id === MafiosoId(id)).filter(_.status === Status.Imprisoned) join
        prisonRecords.filter(_.id === MafiosoId(id)).filter(_.releasedAt.isEmpty)).result.headOption

      exDirectSubordinates <- if (prisoned.isDefined) {
        mafiosos.filter(_.id.inSet(prisoned.get._2.exsubordinates.toSet)).result
      } else DBIOAction.successful(Seq.empty)

      exsubordinates <- if (exDirectSubordinates.nonEmpty) {
        DBIOAction.sequence(exDirectSubordinates.map(e => mafiosos.filter(_.path like s"${e.path}%").result))
      } else DBIOAction.successful(Seq.empty)

      _ <- if (exsubordinates.nonEmpty) {
        val targetPath = Utils.withPath(prisoned.get._1.path, prisoned.get._1.id.get.id.toString)
        DBIOAction.sequence(exsubordinates.flatMap(_.map { e =>
          mafiosos.filter(_.id === e.id)
            .update(e.copy(path = e.path.replaceFirst(e.path, targetPath),
              updatedAt = Some(Timestamp.from(Instant.now))))
        }))
      } else DBIOAction.successful(Seq.empty)

      _ <- if (prisoned.isDefined) {
        val mafioso = prisoned.get._1
        val prisonRecord = prisoned.get._2
        DBIOAction.seq(
          prisonRecords.filter(_.id === prisonRecord.id).filter(_.releasedAt.isEmpty)
            .update(prisonRecord.copy(releasedAt = Some(Timestamp.from(Instant.now)))),
          mafiosos.filter(_.id === mafioso.id).update(mafioso.copy(
            status = Status.Free, updatedAt = Some(Timestamp.from(Instant.now))))
        )
      } else DBIOAction.successful(prisoned)

    } yield prisoned

    db.run(query.transactionally).map(_.flatMap(_._1.id.map(_.id)))
  }

}

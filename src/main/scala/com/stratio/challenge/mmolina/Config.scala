package com.stratio.challenge.mmolina

import com.stratio.challenge.mmolina.persistence.jdbc.MafiosoJdbc
import com.stratio.challenge.mmolina.services.{MafiosoService, SwaggerDocService}
import com.typesafe.config.{ConfigFactory, ConfigResolveOptions}
import org.flywaydb.core.Flyway
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

trait ServerConfig {

  type TypeSafeConfig = com.typesafe.config.Config

  def baseConfig: TypeSafeConfig

  val httpsEnabled = {
    if (baseConfig.hasPath("akka.http.tls.enabled")) baseConfig.getBoolean("akka.http.tls.enabled")
    else false
  }
  lazy val httpsKeystore = baseConfig.getString("akka.http.tls.keystore")
  lazy val httpsKeystorePassword = baseConfig.getString("akka.http.tls.keystore-password")
  lazy val httpsTruststore = baseConfig.getString("akka.http.tls.truststore")
  lazy val httpsTruststorePassword = baseConfig.getString("akka.http.tls.truststore-password")

  private val httpConfig: TypeSafeConfig = baseConfig.getConfig("mafia.server")
  val httpInterface = httpConfig.getString("host")
  val httpPort = httpConfig.getInt("port")


  private val databaseConfig: TypeSafeConfig = baseConfig.getConfig("mafia.database")
  val database: DatabaseConfig[JdbcProfile] =
    DatabaseConfig.forConfig(databaseConfig.getString("config"), databaseConfig)

  val dbProps = s"${databaseConfig.getString("config")}.db.properties"
  if (databaseConfig.hasPath(dbProps)) {
    if (databaseConfig.hasPath(dbProps + ".url")) {
      val url = databaseConfig.getString(dbProps + ".url")
      val user = {
        if (databaseConfig.hasPath(dbProps + ".user")) databaseConfig.getString(dbProps + ".user")
        else ""
      }
      val password = {
        if (databaseConfig.hasPath(dbProps + ".password")) databaseConfig.getString(dbProps + ".password")
        else ""
      }
      val flyway = Flyway.configure.dataSource(url, user, password).load()
      flyway.migrate()
    }
  }
}


class Wirer(implicit ec: ExecutionContext) extends Routes with ServerConfig {

  def baseConfig: TypeSafeConfig = ConfigFactory
    .load(getClass.getClassLoader,
      ConfigResolveOptions.defaults.setAllowUnresolved(true))
    .resolve

  override val mafiosoService: MafiosoService = new MafiosoService(new MafiosoJdbc(database))

  override val swaggerService: SwaggerDocService = new SwaggerDocService(httpInterface, httpPort)

  override def config: ServerConfig = this
}

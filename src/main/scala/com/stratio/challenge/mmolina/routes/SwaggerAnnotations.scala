package com.stratio.challenge.mmolina.routes

import akka.http.scaladsl.server.Route
import com.stratio.challenge.mmolina.domain.{MafiosoResponse, Version}
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs._

sealed trait SwaggerAnnotations

@Path("api/v1/version")
trait VersionRouteAnnotations extends SwaggerAnnotations {

  @GET
  @Operation(summary = "Server version", description = "Return the server version with its build date",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Version response",
        content = Array(new Content(schema = new Schema(implementation = classOf[Version])))),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def getVersion: Route

}

@Path("api/v1/mafiosos")
trait MafiosoRouteAnnotations extends SwaggerAnnotations {

  @POST
  @Operation(summary = "Register a new mafioso", description = "Add a new mafioso to the system",
    responses = Array(
      new ApiResponse(responseCode = "201", description = "Mafioso record created successfully"),
      new ApiResponse(responseCode = "403", description = "The record could not be created due to a preconditions"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def register: Route

  @PUT
  @Operation(summary = "Update a registered mafioso", description = "Update a mafioso from the system",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Mafioso record updated successfully"),
      new ApiResponse(responseCode = "403", description = "The record could not be created due to a preconditions"),
      new ApiResponse(responseCode = "404", description = "The record to update was not found"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def update(id: Int): Route

  @DELETE
  @Operation(summary = "Delete a mafioso", description = "Remove a mafioso from the system",
    responses = Array(
      new ApiResponse(responseCode = "204", description = "Mafioso record removed successfully"),
      new ApiResponse(responseCode = "404", description = "The record to delete was not found"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def unregister(id: Int): Route

  @GET
  @Operation(summary = "Return all registered mafiosos", description = "List all mafiosos in the database using pagination",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Mafioso list response",
        content = Array(new Content(schema = new Schema(implementation = classOf[Seq[MafiosoResponse]])))),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def getMafiosos: Route

  @GET
  @Operation(summary = "Return a mafioso", description = "Look for a mafioso by id.",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Mafioso response",
        content = Array(new Content(schema = new Schema(implementation = classOf[MafiosoResponse])))),
      new ApiResponse(responseCode = "404", description = "The record to get was not found"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def mafiosoById(id: Int): Route

  @GET
  @Operation(summary = "Return all subordinates for a given mafioso",
    description = "List all subordinates for a given mafioso",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Mafioso list response with the subordinates",
        content = Array(new Content(schema = new Schema(implementation = classOf[Seq[MafiosoResponse]])))),
      new ApiResponse(responseCode = "404", description = "The record to get the subordinates was not found"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def getSubordinates(id: Int): Route

  @POST
  @Operation(summary = "Register a mafioso into prison", description = "Update a mafioso record putting into prison",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Mafioso record updated successfully"),
      new ApiResponse(responseCode = "403", description = "The record could not be created due to a preconditions"),
      new ApiResponse(responseCode = "404", description = "The record to update was not found"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def imprison(id: Int): Route

  @POST
  @Operation(summary = "Release a mafioso from prison", description = "Update a mafioso record releasing from prison",
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Mafioso record updated successfully"),
      new ApiResponse(responseCode = "403", description = "The record could not be created due to a preconditions"),
      new ApiResponse(responseCode = "404", description = "The record to update was not found"),
      new ApiResponse(responseCode = "500", description = "Internal server error"))
  )
  def release(id: Int): Route

}

package com.stratio.challenge.mmolina.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{extractRequestContext, handleExceptions, handleRejections, mapResponse, _}
import akka.http.scaladsl.server.{ExceptionHandler, RejectionHandler}
import com.typesafe.scalalogging.LazyLogging

trait RequestWrapper extends LazyLogging {

  private val exceptionHandler = ExceptionHandler {
    case e: Exception =>
      logger.error(s"Exception processing request: ${e.getMessage}", e)
      _.complete(StatusCodes.InternalServerError, "Internal Server Error")
  }

  private val rejectionHandler = RejectionHandler.default

  private val logDuration = extractRequestContext.flatMap { ctx =>
    val start = System.currentTimeMillis()
    mapResponse { resp =>
      val d = System.currentTimeMillis() - start
      logger.info(s"[${resp.status.intValue()}] ${ctx.request.method.name} ${ctx.request.uri} took: ${d}ms")
      resp
    } & handleRejections(rejectionHandler)
  }

  val requestWrapper = logDuration &
    handleExceptions(exceptionHandler) &
    encodeResponse

}

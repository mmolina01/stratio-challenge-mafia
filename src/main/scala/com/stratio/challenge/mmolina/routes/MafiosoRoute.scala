package com.stratio.challenge.mmolina.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.stratio.challenge.mmolina.common.JsonSupport
import com.stratio.challenge.mmolina.domain.MafiosoEntryPoint
import com.stratio.challenge.mmolina.services.MafiosoService
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success}

trait MafiosoRoute extends MafiosoRouteAnnotations with JsonSupport with LazyLogging {

  def mafiosoService: MafiosoService

  lazy val mafioso = pathPrefix("mafiosos") {
    concat(
      pathEndOrSingleSlash(register),
      path(IntNumber)(unregister(_)),
      path(IntNumber)(update(_)),
      pathEndOrSingleSlash(getMafiosos),
      path(IntNumber)(mafiosoById(_)),
      pathPrefix(IntNumber / "subordinates")(getSubordinates(_)),
      pathPrefix(IntNumber / "imprison")(imprison(_)),
      pathPrefix(IntNumber / "release")(release(_))
    )
  }

  override def register: Route = post {
    entity(as[MafiosoEntryPoint]) { mafioso =>
      logger.debug(s"Creating a new mafioso '$mafioso'.")
      onComplete(mafiosoService.register(mafioso)) {
        case Success(s) if s.isDefined => complete(StatusCodes.Created)
        case Success(s) => complete(StatusCodes.Forbidden ->
          s"Cannot create the record due to the boss does not exist.")
        case Failure(e) => complete(StatusCodes.InternalServerError, e.getMessage)
      }
    }
  }

  override def update(id: Int): Route = put {
    entity(as[MafiosoEntryPoint]) { mafioso =>
      if (mafioso.boss.isDefined && mafioso.boss.get == id) {
        complete(StatusCodes.BadRequest, "A mafioso cannot be his own boss")
      } else {
        logger.debug(s"Updating a mafioso '$mafioso'.")
        onComplete(mafiosoService.update(id, mafioso)) {
          case Success(None) =>
            complete(StatusCodes.BadRequest -> s"Mafioso could not be updated (his boss does not exist).")
          case Success(s) if s.get == 0 => complete(StatusCodes.NotFound, s"Record not found.")
          case Success(s) => complete(StatusCodes.OK, s"Record with id '${id}' was updated.")
          case Failure(e) => complete(StatusCodes.InternalServerError, e.getMessage)
        }
      }
    }
  }

  override def unregister(id: Int): Route = delete {
    logger.debug(s"Removing mafioso with id '${id}':")
    onComplete(mafiosoService.unregister(id)) {
      case Success(s) if s == 0 => complete(StatusCodes.NotFound, s"Record not found.")
      case Success(s) => complete(StatusCodes.OK)
      case Failure(e) => complete(StatusCodes.InternalServerError, e.getMessage)
    }
  }

  override def getMafiosos: Route = get {
    parameters('sort.?, 'page.as[Int].?, 'pageSize.as[Int].?) { (sort, page, pageSize) =>
      logger.debug(s"Querying all mafiosos...")
      val mafiosos = mafiosoService.getAll(page, pageSize, sort)
      complete(mafiosos)
    }
  }

  override def mafiosoById(id: Int): Route = get {
    logger.debug(s"Querying mafioso by id '${id}':")
    onComplete(mafiosoService.get(id)) {
      case Success(s) if s.isEmpty => complete(StatusCodes.NotFound, s"Record not found.")
      case Success(s) => complete(s.get)
      case Failure(e) => complete(StatusCodes.InternalServerError, e.getMessage)
    }
  }

  override def getSubordinates(id: Int): Route = get {
    logger.debug(s"Querying subordinates for mafioso with id '${id}':")
    val optSubordinates = mafiosoService.subordinates(id)
    rejectEmptyResponse {
      complete(optSubordinates)
    }
  }

  override def imprison(id: Int): Route = post {
    logger.debug(s"Imprisoning mafioso with id '${id}':")
    onComplete(mafiosoService.imprison(id)) {
      case Success(s) if s.isDefined => complete(StatusCodes.OK)
      case Success(s) => complete(StatusCodes.Forbidden ->
        s"Cannot imprison the mafioso due to does not exist or is in prison.")
      case Failure(e) => complete(StatusCodes.InternalServerError, e.getMessage)
    }
  }

  override def release(id: Int): Route = post {
    logger.debug(s"Releasing mafioso with id '${id}':")
    onComplete(mafiosoService.release(id)) {
      case Success(s) if s.isDefined => complete(StatusCodes.OK)
      case Success(s) => complete(StatusCodes.Forbidden ->
        s"Cannot release the mafioso due to does not exist or is already free.")
      case Failure(e) => complete(StatusCodes.InternalServerError, e.getMessage)
    }
  }

}

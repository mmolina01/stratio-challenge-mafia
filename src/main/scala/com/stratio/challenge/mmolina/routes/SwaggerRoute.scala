package com.stratio.challenge.mmolina.routes

import com.stratio.challenge.mmolina.services.SwaggerDocService
import com.typesafe.scalalogging.LazyLogging

trait SwaggerRoute extends LazyLogging {

  def swaggerService: SwaggerDocService

  lazy val swagger = swaggerService.routes

}

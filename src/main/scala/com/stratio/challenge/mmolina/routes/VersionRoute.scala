package com.stratio.challenge.mmolina.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.stratio.challenge.mmolina.common.JsonSupport
import com.stratio.challenge.mmolina.domain.Version
import com.stratio.challenge.mmolina.version.MafiaBuildInfo

trait VersionRoute extends VersionRouteAnnotations with JsonSupport {

  lazy val version = pathPrefix("version") {
    pathEndOrSingleSlash {
      getVersion
    }
  }

  override def getVersion: Route = get {
    complete(Version(version = MafiaBuildInfo.version, buildDate = MafiaBuildInfo.buildDate))
  }

}

package com.stratio.challenge.mmolina.services

import com.stratio.challenge.mmolina.common.Utils
import com.stratio.challenge.mmolina.domain.{MafiosoEntryPoint, MafiosoId, MafiosoResponse}
import com.stratio.challenge.mmolina.persistence.Mafioso
import com.stratio.challenge.mmolina.persistence.dao.MafiosoDao
import com.stratio.challenge.mmolina.services.conversions.mafioso._
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

class MafiosoService(dao: MafiosoDao)(implicit ec: ExecutionContext) extends LazyLogging {

  def register(mafioso: MafiosoEntryPoint): Future[Option[Int]] = dao.add(mafioso.boss, mafioso)

  def update(id: Int, entryPoint: MafiosoEntryPoint): Future[Option[Int]] = {
    val mafioso = toMafioso(entryPoint).copy(id = Some(MafiosoId(id)))
    entryPoint.boss.map { bossId =>
      find(bossId).flatMap {
        _ match {
          case None => Future.successful(None)
          case Some(boss) => dao.update(mafioso.copy(path = Utils.withPath(boss.path, bossId.toString)))
        }
      }
    }.getOrElse(dao.update(mafioso))
  }

  def unregister(id: Int): Future[Int] = dao.delete(id)

  def getAll(page: Option[Int], pageSize: Option[Int], sort: Option[String]): Future[Seq[MafiosoResponse]] = {
    import MafiosoService._
    dao.selectAll(
      page.getOrElse(defaultPage),
      pageSize.getOrElse(defaultPageSize),
      sort.getOrElse(defaultSortField)
    ).map(_.map(toMafiosoResponse(_)))
  }

  private def find(id: Int): Future[Option[Mafioso]] = dao.findById(id)

  def get(id: Int): Future[Option[MafiosoResponse]] = find(id).map(_.map(toMafiosoResponse(_)))

  private def subs(id: Int): Future[Option[Seq[Mafioso]]] = {
    val result = for {
      mafioso <- find(id)
      subordinates <- if (mafioso.isDefined) dao.subordinates(mafioso.get.path)
      else Future.successful(Seq.empty)
    } yield (mafioso, subordinates)

    result.map(r => if (r._1.isDefined) Some(r._2) else None)
  }

  def subordinates(id: Int): Future[Option[Seq[MafiosoResponse]]] = subs(id).map(_.map(_.map(toMafiosoResponse(_))))

  def imprison(id: Int): Future[Option[Int]] = dao.imprison(id)

  def release(id: Int): Future[Option[Int]] = dao.release(id)

}

object MafiosoService {

  val defaultSortField = "ID"
  val defaultPage = 0
  val defaultPageSize = 50

}

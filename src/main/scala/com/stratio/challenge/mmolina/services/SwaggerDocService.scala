package com.stratio.challenge.mmolina.services

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.{Contact, Info}
import com.stratio.challenge.mmolina.routes.{MafiosoRoute, VersionRoute}
import com.stratio.challenge.mmolina.version.MafiaBuildInfo

class SwaggerDocService(address: String, port: Int) extends SwaggerHttpService {

  override val apiClasses: Set[Class[_]] = Set(classOf[VersionRoute], classOf[MafiosoRoute])

  override val host = address + ":" + port

  override val info = Info(
    description = "Stratio Challenge - Mafia",
    version = MafiaBuildInfo.version,
    title = "Mafia Challenge",
    termsOfService = "Testing",
    contact = Some(Contact(name = "Mario Molina", url = "https://github.com/mmolimar", email = "mmolina@stratio.com"))
  )

  override val apiDocsPath = "api-docs"

}

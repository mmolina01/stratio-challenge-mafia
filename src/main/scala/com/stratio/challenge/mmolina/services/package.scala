package com.stratio.challenge.mmolina

import com.stratio.challenge.mmolina.common.{Constants, Utils}
import com.stratio.challenge.mmolina.domain.{MafiosoEntryPoint, MafiosoId, MafiosoResponse}
import com.stratio.challenge.mmolina.persistence.Mafioso

package object services {

  object conversions {

    object mafioso {

      implicit def toMafioso(entry: MafiosoEntryPoint): Mafioso = {
        Mafioso(
          name = entry.name,
          surname = entry.surname,
          birthDate = entry.birthDate
        )
      }

      implicit def toMafiosoResponse(entry: Mafioso): MafiosoResponse = {
        MafiosoResponse(
          id = entry.id.getOrElse(MafiosoId(-1)).id,
          name = entry.name,
          surname = entry.surname,
          level = entry.path.count(_ == Constants.PathSeparator) - 1,
          bossId = Utils.getBoss(entry.path),
          birthDate = entry.birthDate,
          status = entry.status.toString.toUpperCase,
          createdAt = entry.createdAt,
          updatedAt = entry.updatedAt
        )
      }
    }

  }

}

package com.stratio.challenge.mmolina.domain

case class Version(version: String, buildDate: String)

package com.stratio.challenge.mmolina.domain

import java.sql.Timestamp
import java.time.LocalDate

case class MafiosoId(id: Int)

case class MafiosoEntryPoint(
                              name: String,
                              surname: String,
                              boss: Option[Int] = None,
                              birthDate: Option[LocalDate] = None
                            )

case class MafiosoResponse(
                            id: Int,
                            name: String,
                            surname: String,
                            level: Int,
                            bossId: Option[Int],
                            birthDate: Option[LocalDate],
                            status: String,
                            createdAt: Timestamp,
                            updatedAt: Option[Timestamp]
                          )
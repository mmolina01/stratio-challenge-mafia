package com.stratio.challenge.mmolina

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.stratio.challenge.mmolina.common.{Constants, SSLManager}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Mafia extends App with LazyLogging {

  implicit val system = ActorSystem(Constants.MafiaActorySystem)
  implicit val executor: ExecutionContext = system.dispatcher
  implicit val materializer = ActorMaterializer()

  logger.info(s"Starting Mafia server...")
  val wirer = new Wirer
  val config = wirer.config
  val bindingFuture = if (config.httpsEnabled) {
    val https = new SSLManager(config.httpsKeystore, config.httpsKeystorePassword,
      config.httpsTruststore, config.httpsTruststorePassword).https
    Http().setDefaultServerHttpContext(https)
    Http().bindAndHandle(wirer.routes, config.httpInterface, config.httpPort, connectionContext = https)
  } else {
    Http().bindAndHandle(wirer.routes, config.httpInterface, config.httpPort)
  }

  bindingFuture.onComplete {
    case Success(b) =>
      logger.info(s"Mafia server started on ${config.httpInterface}:${config.httpPort}")
      sys.addShutdownHook {
        b.unbind()
        system.terminate()
        logger.info("Server stopped")
      }
    case Failure(e) =>
      logger.error(s"Unexpected error starting server on ${config.httpInterface}:${config.httpPort}", e)
      sys.addShutdownHook {
        system.terminate()
        logger.info("Server stopped")
      }
  }
}

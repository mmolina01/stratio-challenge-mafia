package com.stratio.challenge.mmolina.common

import com.stratio.challenge.mmolina.common.Constants.PathSeparator

object Utils {

  def withPath(left: String, right: String, separator: Char = PathSeparator): String = {
    val separator = PathSeparator
    val urlEndsInSlash = left.lastOption.contains(separator)
    val suffixBeginsWithSlash = right.headOption.contains(separator)
    val result = if (urlEndsInSlash && suffixBeginsWithSlash) {
      left + right.tail
    } else if (urlEndsInSlash || suffixBeginsWithSlash) {
      left + right
    } else if (right.isEmpty) {
      left
    } else {
      left + separator + right
    }

    val prefix = if (result.headOption.contains(separator)) "" else separator
    val suffix = if (result.lastOption.contains(separator)) "" else separator

    prefix  + result + suffix
  }

  private val pathRegex = ".*/([\\d]+)/{0,1}".r

  def getBoss(path: String): Option[Int] = {
    path match {
      case pathRegex(d) => Some(d.toInt)
      case _ => None
    }
  }

}

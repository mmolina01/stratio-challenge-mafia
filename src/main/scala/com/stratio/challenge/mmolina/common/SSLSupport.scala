package com.stratio.challenge.mmolina.common

import java.io.{FileInputStream, InputStream}
import java.security.{KeyStore, SecureRandom}

import akka.http.scaladsl.{ConnectionContext, HttpsConnectionContext}
import javax.net.ssl._

case class SSLManager(httpsKeyStore: String, httpsKeyStorePwd: String,
                      httpsTrustStore: String, httpsTrustPwd: String) {

  import SSLManager._

  lazy val https: HttpsConnectionContext = ConnectionContext.https(createSSLContext(this))
}

object SSLManager {

  protected def createSSLContext(manager: SSLManager): SSLContext = {
    val sslContext: SSLContext = SSLContext.getInstance("TLS")

    val keyManagerFactory = getKeyManagerFactory(manager.httpsKeyStore, manager.httpsKeyStorePwd)
    val trustManagerFactory = getTrustManagerFactory(manager.httpsTrustStore, manager.httpsTrustPwd)

    sslContext.init(keyManagerFactory.getKeyManagers,
      trustManagerFactory.getTrustManagers, new SecureRandom())

    sslContext
  }

  def getKeyManagerFactory(keyStorePath: String, password: String): KeyManagerFactory = {
    val keyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(getKeyStore(keyStorePath, password), password.toCharArray)
    keyManagerFactory
  }

  def getTrustManagerFactory(trustStorePath: String, password: String): TrustManagerFactory = {
    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    tmf.init(getKeyStore(trustStorePath, password))
    tmf
  }

  def getKeyStore(keyStorePath: String, password: String): KeyStore = {
    val ks: KeyStore = KeyStore.getInstance("JKS")
    val keystore: InputStream = new FileInputStream(keyStorePath)
    require(keystore != null, "Keystore required!")
    ks.load(keystore, password.toCharArray)
    ks
  }
}

package com.stratio.challenge.mmolina.common

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDate

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.stratio.challenge.mmolina.domain._
import spray.json._

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  val dateFormatter = new SimpleDateFormat("yyyy-MM-dd")
  val timestampFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

  implicit val dateFormat = new RootJsonFormat[LocalDate] {

    def read(json: JsValue): LocalDate = LocalDate.parse(json.compactPrint)

    def write(date: LocalDate) = JsString(dateFormatter.format(date))
  }

  implicit val timestampFormat = new RootJsonFormat[Timestamp] {

    def read(json: JsValue): Timestamp = json match {
      case js: JsString => Timestamp.from(timestampFormatter.parse(js.value).toInstant)
      case js => Timestamp.from(timestampFormatter.parse(js.compactPrint).toInstant)
    }

    def write(ts: Timestamp) = JsString(timestampFormatter.format(ts.getTime))
  }

  implicit val versionFormat = jsonFormat2(Version)

  implicit val mafiosoEntryPointFormat = jsonFormat4(MafiosoEntryPoint)

  implicit val mafiosoResponseFormat = jsonFormat9(MafiosoResponse)

}

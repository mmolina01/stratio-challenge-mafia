package com.stratio.challenge.mmolina.common

import com.typesafe.config.{ConfigFactory, ConfigResolveOptions}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

trait ServerConfig {

  type TypeSafeConfig = com.typesafe.config.Config
  private val config: TypeSafeConfig = ConfigFactory
    .load(getClass.getClassLoader,
      ConfigResolveOptions.defaults.setAllowUnresolved(true))
    .resolve

  val httpsEnabled = {
    if (config.hasPath("akka.http.tls.enabled")) config.getBoolean("akka.http.tls.enabled")
    else false
  }
  lazy val httpsKeystore = config.getString("akka.http.tls.keystore")
  lazy val httpsKeystorePassword = config.getString("akka.http.tls.keystore-password")
  lazy val httpsTruststore = config.getString("akka.http.tls.truststore")
  lazy val httpsTruststorePassword = config.getString("akka.http.tls.truststore-password")

  private val httpConfig: TypeSafeConfig = config.getConfig("mafia.server")
  val httpInterface = httpConfig.getString("host")
  val httpPort = httpConfig.getInt("port")


  private val databaseConfig: TypeSafeConfig = config.getConfig("mafia.database")
  val database: DatabaseConfig[JdbcProfile] =
    DatabaseConfig.forConfig(databaseConfig.getString("config"), databaseConfig)

}

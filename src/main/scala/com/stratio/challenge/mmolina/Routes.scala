package com.stratio.challenge.mmolina

import akka.http.scaladsl.server.Directives.{pathPrefix, _}
import akka.http.scaladsl.server.Route
import com.stratio.challenge.mmolina.routes._

trait ApiV1 {

  def apiPrefix(r: Route): Route = pathPrefix("api" / "v1")(r)

}

trait Routes extends RequestWrapper
  with SwaggerRoute
  with VersionRoute
  with MafiosoRoute
  with ApiV1 {

  def config: ServerConfig

  private lazy val versioned = apiPrefix {
    concat(
      version,
      mafioso
    )
  }

  lazy val routes = requestWrapper {
    versioned ~
      swagger
  }

}

import java.text.SimpleDateFormat
import java.util.Date

import sbt.Keys._
import sbt._

val projectVersion = "0.1.0-SNAPSHOT"

name := "stratio-challenge-mafia"
version := projectVersion
scalaVersion := "2.12.8"

val typesafeConfigVersion = "1.3.4"
val akkaHttpVersion = "10.1.8"
val slickVersion = "3.3.0"
val h2Version = "1.4.199"
val flywayVersion = "5.2.4"
val rsApiVersion = "2.0.1"
val swaggerAkkaVersion = "2.0.2"
val swaggerModuleVersion = "2.0.4"
val swaggerVersion = "2.0.8"
val scalaLoggingVersion = "3.9.2"
val logbackVersion = "1.2.3"
val slf4jVersion = "1.7.26"
val scalatestVersion = "3.0.7"

initialize := {
  assert(Integer.parseInt(sys.props("java.specification.version").split("\\.")(1)) >= 8, "Java 8 or above required")
}

resolvers += Resolver.mavenLocal

val baseLibs = Seq(
  "com.typesafe" % "config" % typesafeConfigVersion,
  "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion
)
val dbLibs = Seq(
  "com.h2database" % "h2" % h2Version,
  "org.flywaydb" % "flyway-core" % flywayVersion
)
val httpLibs = Seq(
  "javax.ws.rs" % "javax.ws.rs-api" % rsApiVersion,
  "com.github.swagger-akka-http" %% "swagger-akka-http" % swaggerAkkaVersion,
  "com.github.swagger-akka-http" %% "swagger-scala-module" % swaggerModuleVersion,
  "io.swagger.core.v3" % "swagger-core" % swaggerVersion,
  "io.swagger.core.v3" % "swagger-annotations" % swaggerVersion,
  "io.swagger.core.v3" % "swagger-models" % swaggerVersion,
  "io.swagger.core.v3" % "swagger-jaxrs2" % swaggerVersion
)
val loggingLibs = Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion,
  "org.slf4j" % "slf4j-api" % slf4jVersion,
  "ch.qos.logback" % "logback-classic" % logbackVersion
)
val testLibs = Seq (
  "org.scalatest" %% "scalatest" % scalatestVersion % "test",
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion
)

libraryDependencies ++= baseLibs ++ dbLibs ++ httpLibs ++ loggingLibs ++ testLibs

buildInfoPackage := "com.stratio.challenge.mmolina.version"
buildInfoObject := "MafiaBuildInfo"
buildInfoKeys := Seq[BuildInfoKey](
  BuildInfoKey.action("version")(projectVersion),
  BuildInfoKey.action("buildDate")(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()))
)

enablePlugins(BuildInfoPlugin)

mainClass in (Compile, run) := Some("com.stratio.challenge.mmolina.Mafia")

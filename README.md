# Stratio Challenge - Mafia

**stratio-challenge-mafia** is a simple implementation of a REST API for the Stratio Challenge.

## Assumptions

 * The number of records to deal with is big but not that big. The application should manage
 thousands of records but not billions. What this means is that, this is not a Big Data problem.
 * Even though the application is not going to store huge amounts of data, possibly it requires
 manage multiple concurrent requests and a way of scaling easily.
 * The API considers that a *super boss* doesn't exist but it's allowed to have multiple independent
 organizations. So, it's possible to have different mafiosos with no boss with their corresponding
 subordinates.

## About the implementation

The API has been implemented based on the following:
 * **Akka-HTTP**: toolkit for building concurrent and distributed applications.
 * **Slick**: as ORM (by default H2 is used).
 * **Flyway**: for schema migration/evolution.
 * **Swagger-Annotations**: to generate automatically Swagger documentation.

## Getting started

### Building from source ###

Just clone the repo and package it:

``sbt clean package``

If you want to build a fat jar containing both classes and dependencies, type the following:

``sbt clean assembly``

### Running tests ###

To run unit tests, execute the following:

``sbt test``

#### Coverage ###

To know the test coverage:

``sbt clean coverage test coverageReport``

### Running the application ###

To run the application, just type:

``sbt run``

### Default config ###

Running the ``com.stratio.challenge.mmolina.Mafia`` class with the default config, runs the server
a ``http://localhost:8080``.

To see how the API works:
 * Go to: ``http://localhost:8080/api-docs/swagger.yaml`` or ``http://localhost:8080/api-docs/swagger.json``.
 * Copy the content.
 * Open this [link](https://editor.swagger.io/) and paste the content in there.

## TODO's

- [ ] Use Akka actors instead of services for each layer.
- [ ] Add a notification system to improve the surveillance.
- [ ] Add authentication to the HTTP API.
- [ ] Improve manageent of HTTP requests/responses.
- [ ] Use of ``FutureEither`` instead of ``Future`` to manage exceptions in a better way.
- [ ] More unit tests and add integration tests.
